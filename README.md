Gedit PlantUML Plugin
---------------------

[PlantUML](http://www.plantuml.com) is a Java application which enables you to 
'draw' UML diagrams using a scripting language called DOT. A PlantUML script can
be made in any text editor and there are [plenty](http://plantuml.com/running.html) text editors and IDEs that 
support the generation of PlantUML diagrams. This was however not the case for 
[Gedit](https://wiki.gnome.org/Apps/Gedit), the text editor I like to work with. For that reason I developed a 
PlantUML plugin for it. Most Linux-distro's are shipped with Gedit3 nowadays 
(including mine), so I discontinued the development of the plugin for Gedit2.


Releases
--------

Gedit 3: \
[gedit3-plugin-plantuml-2.0.1](http://ruudbeukema.nl/wp-content/uploads/2017/05/gedit3-plugin-plantuml-2.0.1.zip)


Gedit 2 (development discontinued): \
[gedit2-plugin-plantuml-2.0.0-devel-1](http://ruudbeukema.nl/wp-content/uploads/2016/05/gedit2-plugin-plantuml-2.0.0-devel-1.zip)


Installation
------------

The plugin is not shipped with Gedit by default and must be installed manually. 
The plugin uses PlantUML and ships this Java application. In order to be able 
to use PlantUML Java is required. Apart from that, PlantUML also requires 
[Graphviz](http://www.graphviz.org) to be installed. It uses it for generating its diagrams.

**Linux**

Unpack the zip archive and copy the contents of the folder

`/src`

to your home folder: (for gedit-2):

`/home/username/.gnome2/gedit/plugins`

(for gedit-3):

`/home/username/.local/share/gedit/plugins`

or, in order to use it system-wide, to the folder: (for gedit-2):

`/usr/share/gedit-2/plugins`

(for gedit-3):

`/usr/share/gedit/plugins`


**Windows**

Gedit is primarily developed for Linux and as such there aren't too many 
Windows-installers available for it. I managed to verify that the plugin works 
on Windows using a [test version of the Gedit 3.18.2 Windows-installer](https://people.gnome.org/~icq/gedit-x86_64-3.18.2.msi).